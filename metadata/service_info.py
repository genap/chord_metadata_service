# Service info according to spec https://github.com/ga4gh-discovery/ga4gh-service-info

SERVICE_INFO = {
	"id": "ca.distributedgenomics.chord_metadata_service",
	"name": "Metadata Service",
	"type": "data service",
	"description": "Metadata service implementation based on Phenopackets schema",
	"organization": {
		"name": "C3G",
		"url": "http://www.computationalgenomics.ca/"
	},
	"contactUrl": "mailto:ksenia.zaytseva@mcgill.ca",
	"version": "TODO"
}